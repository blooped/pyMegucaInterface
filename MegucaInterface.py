#!/bin/python3.6
from datetime import datetime
import pytz
import requests
import json
from feedgen.feed import FeedGenerator
from requests_futures.sessions import FuturesSession
import argparse

class MegucaInterface():
    def __init__(self):
        self.time_zone = "America/New_York"
        self.timezone = pytz.timezone(self.time_zone)
        self.max_worker_threads = 10
        self.base_url = "https://meguca.org"
        
        self.fg = FeedGenerator()
    
    ''' Test if user's list of boards exist on site. Returns boolean '''
    def isBoardListValid(self, board_list):
        for board in board_list:
            exists = False
            for board_avail in self.getBoards():
                if board_avail['id'] == board:
                    exists = True
            if exists == False:
                print("Error: Board "+ board + " does not exist")
                return exists
        return exists
    
    ''' Get and return json obj of the board list available on the server '''
    def getBoards(self):
        boards_api = requests.get(self.base_url + "/json/board-list")
        boards_obj = json.loads(boards_api.content)
        boards_obj.append({"id":"all","title":"All"})
        return boards_obj
        
    ''' Get and return the json obj of valid file extensions allowed attached in a post, used for processing attachments'''
    def getFileExtensions(self):
        file_ext_api = requests.get(self.base_url + "/json/extensions")
        file_ext_obj = json.loads(file_ext_api.content)
        return file_ext_obj
    
    ''' Get and return unique IP user count '''
    def getUniqueIPCount(self):
        unique_ip_count_api = requests.get(self.base_url + "/json/ip-count")
        unique_ip_count_obj = json.loads(unique_ip_count_api.content) 
        return unique_ip_count_obj 
    
    ''' Get and return json object containing the threads in the catalog but not including the reply posts '''
    def getBoardCatalog(self, board):
        board_catalog_api = requests.get(self.base_url + '/json/boards/'+ board +'/catalog')
        board_catalog_obj = json.loads(board_catalog_api.content)
        return board_catalog_obj
        
    ''' Get and return list of hyperlinks to the OP threads in the catalog'''
    def getBoardCatalogLinks(self, board):
        returnList = []
        for op in self.getBoardCatalog(board):
            returnList.append(self.base_url + "/" + board + "/" + str(op['id']) )
        return returnList
    
    ''' Get and return the entire thread or last N posts '''
    def getThread(self, board, thread, count='None', session='None'):
        if session == 'None':
            if count == 'None':    
                get_thread_api = requests.get(self.base_url + "/json/boards/" + board + "/" + str(thread))
            else:
                get_thread_api = requests.get(self.base_url + "/json/boards/" + board + "/" + str(thread) + "?last=" + count)
            get_thread_obj = json.loads(get_thread_api.content) 
            return get_thread_obj
        else:
            if count == 'None':    
                get_thread_api = session.get(self.base_url + "/json/boards/" + board + "/" + str(thread))
            else:
                get_thread_api = session.get(self.base_url + "/json/boards/" + board + "/" + str(thread) + "?last=" + count)            
            return get_thread_api
        
        ''' Get every post from board, or every OP and last N posts, uses FutureSession to process gets in parallel '''
    def getThreadsAndReplies(self, board, count='None'):
        returnObj = []
        board_catalog = self.getBoardCatalog(board)
        futurePool = []
        idx = 0
        
        with FuturesSession(max_workers=self.max_worker_threads) as session:                   
            for article in board_catalog:
                if count == 'None':
                    futurePool.append(self.getThread(article['board'], article['id'], session=session)) 
                else:
                    futurePool.append(self.getThread(article['board'], article['id'], session=session)) 
                
            for future in futurePool:
                response = future.result() # Block until session is done
                get_thread_obj = json.loads(response.content) 
                
                board_catalog[idx]['replies'] = get_thread_obj['posts']
                returnObj.append(article) 
                idx=idx+1
                
        return returnObj

    
    ''' Get and return RSS feed for boards in a list. RSS doesn't contain thread replies, only contains the OPs '''
    def getRSS(self, board_list):
        return self.generateFeed(board_list).decode("utf-8")
    
    def printThreadsAndReplies(self, board_list):
        for board in board_list:
            print(str(self.getThreadsAndReplies(board)))
        
    def printUniqueIPCount(self):
        print(str(self.getUniqueIPCount()))
        
    def printBoardsCatalogVerbose(self, board_list):
        for board in board_list:
            print(self.getBoardCatalog())
        
    def printBoards(self):
        for board_avail in self.getBoards():
            print(board_avail)
                
    def printRSS(self, board_list):
        print(self.generateFeed(board_list).decode("utf-8"))
        
    def writeRSSFile(self, board_list, file_name):
        if file_name != "None":
            #if not hasattr(self, "fg"):
            self.generateFeed(board_list)
            self.fg.rss_file(file_name[0]) # Write out to file
                    
    def generateFeed(self, board_list):
        self.fg.id('https://meguca.org')
        self.fg.logo('https://meguca.org/assets/favicons/default.ico')
        self.fg.link(href='http://meguca.org', rel='alternate')
        self.fg.subtitle('Meguca catalog feed')
        self.fg.language('en')   
        title = ""
        
        for board in board_list:
            title = board + " " +  title
            board_catalog_api = requests.get(self.base_url + '/json/boards/'+ board +'/catalog')
            pageObj = json.loads(board_catalog_api.content)
            # Create RSS entry for each json entry, add attachment
            for article in pageObj:
                entry = self.fg.add_entry()
                entry.published(datetime.fromtimestamp(article['time'], self.timezone))
                entry.title(article['subject'])
                entry.description(article['body'])
                entry.id(self.base_url +"/" + article['board']+"/"+str(article['id']))
                entry.link(href=self.base_url +"/" + article['board']+"/"+str(article['id']), rel="alternate")
                entry.content(content=article['body'], src=self.base_url +"/"+ article['board']+"/"+str(article['id']))
                try:
                    entry.enclosure(url=self.base_url + "/assets/images/src/"+article['image']['SHA1']+"." + self.getFileExtensions()[str(article['image']['fileType'])], 
                                    length=str(article['image']['size']), type=self.getFileExtensions()[str(article['image']['fileType'])])
                except: 
                    pass
        
        self.fg.title("Meguca " + title + " feed")	
        
        rss_feed  = self.fg.rss_str(pretty=True) # Get the RSS feed as string
        return rss_feed
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', nargs='*', help='Select single or multiple boards, space seperated: e.g. "-b g pol polk", default:"-b all"', default=['all'])
    parser.add_argument('-t', nargs='*', help='Print table of available boards', default="False")
    parser.add_argument('-f', nargs='*', help='Save to file: -f name.rss', default="None")
    parser.add_argument('-o', nargs='*', help='Output to STDOUT', default="False")
    parser.add_argument('-c', nargs='*', help='Get unique ip count', default="False")
    parser.add_argument('-e', nargs='*', help='Output Print catalog including replies to STDOUT', default="False")
    parser.add_argument('-v', nargs='*', help='Output object to STDOUT', default="False")
    args = parser.parse_args()

    mi = MegucaInterface()
    if 'b' in args:
        if not mi.isBoardListValid(args.b):
            exit()

    if 't' in args:
        if args.t != 'False':
            mi.printBoards()
            exit()
            
    if 'f' in args:
        if args.f == []:
            print("Please enter filename")
            exit()
        elif args.f != "None":
            mi.writeRSSFile(args.b, args.f)
                
    if 'o' in args:
        if args.o != 'False':
            mi.printRSS(args.b)
            
    if 'c' in args:
        if args.c != 'False':            
            mi.printUniqueIPCount()
            
    if 'v' in args:
        if args.v != 'False':
            mi.printBoardsCatalogVerbose(args.b)
    
    if 'e' in args:
        if args.e != 'False':
            mi.printThreadsAndReplies(args.b)

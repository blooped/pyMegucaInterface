
```
To run from terminal in linux:
chmod a+x  ./MegucaInterface.py
./MegucaInterface.py -b g a -o

Print help to get commands available:
./MegucaInterface.py --help
usage: MegucaInterface.py [-h] [-b [B [B ...]]] [-t [T [T ...]]]
                          [-f [F [F ...]]] [-o [O [O ...]]] [-c [C [C ...]]]
                          [-v [V [V ...]]]

optional arguments:
  -h, --help      show this help message and exit
  -b [B [B ...]]  Select single or multiple boards, space seperated: e.g. "-b g a", "-b all" is default
  -t [T [T ...]]  Print table of available boards
  -f [F [F ...]]  Save to file: -f name.rss
  -o [O [O ...]]  Output to STDOUT
  -c [C [C ...]]  Get unique ip count
  -v [V [V ...]]  Output object to STDOUT

Examples:
Use script to output only rss feed for consumption by lifrea or similar feedreader for b, g and a boards:
MegucaInterface.py -b g a b -o

The following will output to output.xml file an rss for all catalog:
MegucaInterface.py -f output.xml

The following just outputs the unique ip count
MegucaInterface.py -c

pyMegucaInterface class can also be called by other classes, for use by UI's for instance.
```
